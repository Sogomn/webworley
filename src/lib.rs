#![allow(dead_code, unused_imports)]

#[macro_use]
extern crate stdweb;

use std::f64;
use std::io::Write;
use worley_noise::WorleyNoise;
use gif::{
	Encoder,
	Frame,
	ExtensionData,
	Repeat
};

const DISTANCE_FN_EUCLIDEAN_SQ: &Fn(f64, f64, f64) -> f64 = &|x, y, z| {
	x * x + y * y + z * z
};
const DISTANCE_FN_MANHATTAN: &Fn(f64, f64, f64) -> f64 = &|x, y, z| {
	x.abs() + y.abs() + z.abs()
};
const DISTANCE_FN_CHEBYSHEV: &Fn(f64, f64, f64) -> f64 = &|x, y, z| {
	x.abs().max(y.abs()).max(z.abs())
};
const VALUE_FN_BLOBS: &Fn(Vec<f64>) -> f64 = &|distances| {
	let mut min = f64::MAX;
	
	for &distance in distances.iter() {
		if distance < min {
			min = distance;
		}
	}
	
	min
};
const VALUE_FN_CRYSTALS: &Fn(Vec<f64>) -> f64 = &|distances| {
	let mut min = f64::MAX;
	let mut second_min = f64::MAX;
	
	for &distance in distances.iter() {
		if distance < min {
			second_min = min;
			min = distance;
		} else if distance < second_min {
			second_min = distance;
		}
	}
	
	second_min - min
};
const VALUE_FN_CRYSTALS_2: &Fn(Vec<f64>) -> f64 = &|distances| {
	let mut min = f64::MAX;
	let mut second_min = f64::MAX;
	let mut third_min = f64::MAX;
	
	for &distance in distances.iter() {
		if distance < min {
			third_min = second_min;
			second_min = min;
			min = distance;
		} else if distance < second_min {
			third_min = second_min;
			second_min = distance;
		} else if distance < third_min {
			third_min = distance;
		}
	}
	
	third_min - (second_min + min)
};
const VALUE_FN_CRYSTALS_3: &Fn(Vec<f64>) -> f64 = &|distances| {
	let mut min = f64::MAX;
	let mut second_min = f64::MAX;
	let mut third_min = f64::MAX;
	
	for &distance in distances.iter() {
		if distance < min {
			third_min = second_min;
			second_min = min;
			min = distance;
		} else if distance < second_min {
			third_min = second_min;
			second_min = distance;
		} else if distance < third_min {
			third_min = distance;
		}
	}
	
	second_min - min + third_min
};
const VALUE_FN_SPARKS: &Fn(Vec<f64>) -> f64 = &|distances| {
	let mut min = f64::MAX;
	let mut second_min = f64::MAX;
	let mut third_min = f64::MAX;
	
	for &distance in distances.iter() {
		if distance < min {
			third_min = second_min;
			second_min = min;
			min = distance;
		} else if distance < second_min {
			third_min = second_min;
			second_min = distance;
		} else if distance < third_min {
			third_min = distance;
		}
	}
	
	(min + second_min - third_min).max(0.0)
};
const VALUE_FN_BLOBS_2: &Fn(Vec<f64>) -> f64 = &|distances| {
	let mut min = f64::MAX;
	let mut second_min = f64::MAX;
	
	for &distance in distances.iter() {
		if distance < min {
			second_min = min;
			min = distance;
		} else if distance < second_min {
			second_min = distance;
		}
	}
	
	min + second_min
};

fn create_color_table(colors: &[u32], relative_distribution: bool) -> Vec<(u8, u8, u8)> {
	let mut table = Vec::new();
	
	for i in 0 .. colors.len() - 1 {
		let (current_r, current_g, current_b) = {
			let rgb = colors[i];
			let r = (rgb >> 16) & 0xff;
			let g = (rgb >> 8) & 0xff;
			let b = rgb & 0xff;
			
			(r as f64, g as f64, b as f64)
		};
		let (next_r, next_g, next_b) = {
			let rgb = colors[i + 1];
			let r = (rgb >> 16) & 0xff;
			let g = (rgb >> 8) & 0xff;
			let b = rgb & 0xff;
			
			(r as f64, g as f64, b as f64)
		};
		let (diff_r, diff_g, diff_b) = (
			next_r - current_r,
			next_g - current_g,
			next_b - current_b
		);
		let diff_max = if relative_distribution {
			diff_r.abs().max(diff_g.abs()).max(diff_b.abs())
		} else {
			255.0
		};
		let (change_r, change_g, change_b) = (
			diff_r / diff_max,
			diff_g / diff_max,
			diff_b / diff_max
		);
		
		for j in 0 .. diff_max.ceil() as i64 {
			let r = (current_r as f64 + (change_r * j as f64))
				.min(255.0)
				.max(0.0) as u8;
			let g = (current_g as f64 + (change_g * j as f64))
				.min(255.0)
				.max(0.0) as u8;
			let b = (current_b as f64 + (change_b * j as f64))
				.min(255.0)
				.max(0.0) as u8;
			
			table.push((r, g, b));
		}
	}
	
	table
}

#[js_export]
pub fn generate_noise_images(width: u32, height: u32, scale: f64, colors: &[u32], relative_color_distribution: bool, distance_fn_idx: u8, value_fn_idx: u8, range: f64, step: f64) -> Vec<Vec<u8>> {
	let capacity = ((width + 1) as f64 * (height + 1) as f64 * scale) as usize;
	let mut noise = WorleyNoise::with_cache_capacity(capacity);
	let color_table = create_color_table(colors, relative_color_distribution);
	let mut z = 0.0;
	let mut images = Vec::new();
	
	noise.set_distance_function(move |x, y, z| {
		let distance_fn = match distance_fn_idx {
			0 => DISTANCE_FN_EUCLIDEAN_SQ,
			1 => DISTANCE_FN_MANHATTAN,
			2 => DISTANCE_FN_CHEBYSHEV,
			_ => DISTANCE_FN_EUCLIDEAN_SQ
		};
		
		distance_fn(x, y, z)
	});
	noise.set_value_function(move |distances| {
		let value_fn = match value_fn_idx {
			0 => VALUE_FN_BLOBS,
			1 => VALUE_FN_CRYSTALS,
			2 => VALUE_FN_CRYSTALS_2,
			3 => VALUE_FN_CRYSTALS_3,
			4 => VALUE_FN_SPARKS,
			5 => VALUE_FN_BLOBS_2,
			_ => VALUE_FN_BLOBS
		};
		
		value_fn(distances)
	});
	
	if range == 0.0 || step == 0.0 {
		noise.set_radius_z(0);
	}
	
	while z < range || range == 0.0 {
		let mut points = Vec::with_capacity(capacity);
		
		for y in 0 .. height {
			for x in 0 .. width {
				points.push((x as f64 * scale, y as f64 * scale, z));
			}
		}
		
		let values = noise.values_3d(&points);
		let max = values.iter().fold(0.0, |x, y| f64::max(x, *y));
		let mut color_values = Vec::with_capacity(values.len());
		
		for val in values {
			let val = (val / max).abs().min(1.0);
			let index = (val * (color_table.len() - 1) as f64) as usize;
			let (r, g, b) = color_table[index];
			
			color_values.push(r);
			color_values.push(g);
			color_values.push(b);
			color_values.push(0xff);
		}
		
		images.push(color_values);
		
		if range != 0.0 && step != 0.0 {
			z += step;
		} else {
			break;
		}
	}
	
	images
}

#[js_export]
pub fn create_gif(width: u16, height: u16, data: Vec<Vec<u8>>) -> Vec<u8> {
	std::panic::set_hook(Box::new(|p| {
		let msg = format!("{}", p);
		
		js! {
			console.log(@{msg});
		}
	}));
	
	let mut data = data;
	let mut gif_data = Vec::new();
	
	{
		let mut encoder = Encoder::new(&mut gif_data, width, height, &[]).unwrap();
		
		for frame_data in data.iter_mut() {
			let frame = Frame::from_rgba(width, height, frame_data);
			
			encoder.write_frame(&frame).unwrap();
		}
		
		encoder.write_extension(ExtensionData::Repetitions(Repeat::Infinite)).unwrap();
	}
	
	gif_data
}
