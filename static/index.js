const DISTANCE_LABELS = [
	"Euclidean",
	"Manhattan",
	"Chebyshev"
];
const VALUE_LABELS = [
	"Blobs",
	"Crystals",
	"Crystals 2",
	"Crystals 3",
	"Sparks",
	"Blobs 2"
];

const animationsPlaying = [];
let currentAnimationId = 0;

window.addEventListener("load", () => {
	Rust.webworley.then(init).catch(e => console.log(e));
});

function init(worley) {
	const generateButton = document.getElementById("generate");
	const animateCheckbox = document.getElementById("animate");
	const scaleSlider = document.getElementById("scale");
	const distanceSlider = document.getElementById("noise-distance-type");
	const valueSlider = document.getElementById("noise-value-type");
	
	generateButton.addEventListener("click", () => {
		animationsPlaying[currentAnimationId] = false;
		animationsPlaying[++currentAnimationId] = true;
		
		generate(worley);
	});
	animateCheckbox.addEventListener("click", () => {
		if (animateCheckbox.checked) {
			toggleAnimationInput(true);
		} else {
			toggleAnimationInput(false);
		}
	});
	scaleSlider.addEventListener("mousemove", e => {
		const value = parseFloat(scaleSlider.value);
		const min = parseFloat(scaleSlider.min);
		const max = parseFloat(scaleSlider.max);
		const percentage = ((value - min) / (max - min) * (10.0 - 0.1) + 0.1).toFixed(1);
		
		showFloatingLabel(`${percentage}%`);
		moveFloatingLabel(e.clientX, e.clientY);
	});
	distanceSlider.addEventListener("mousemove", e => {
		const value = parseInt(distanceSlider.value);
		const label = DISTANCE_LABELS[value];
		
		showFloatingLabel(label);
		moveFloatingLabel(e.clientX, e.clientY);
	});
	valueSlider.addEventListener("mousemove", e => {
		const value = parseInt(valueSlider.value);
		const label = VALUE_LABELS[value];
		
		showFloatingLabel(label);
		moveFloatingLabel(e.clientX, e.clientY);
	});
	valueSlider.addEventListener("mouseout", () => {
		hideFloatingLabel();
	});
	distanceSlider.addEventListener("mouseout", () => {
		hideFloatingLabel();
	});
	scaleSlider.addEventListener("mouseout", () => {
		hideFloatingLabel();
	});
}

function moveFloatingLabel(x, y) {
	const floatingLabel = document.getElementById("floating-label");
	const labelWidth = floatingLabel.clientWidth;
	const labelHeight = floatingLabel.clientHeight;
	
	floatingLabel.style.left = x - labelWidth / 2;
	floatingLabel.style.top = y - labelHeight * 2;
}

function showFloatingLabel(text) {
	const floatingLabel = document.getElementById("floating-label");
	
	floatingLabel.textContent = text;
	floatingLabel.style.display = "block";
}

function hideFloatingLabel() {
	const floatingLabel = document.getElementById("floating-label");
	
	floatingLabel.style.display = "none";
}

function toggleAnimationInput(show) {
	const animationInput = document.getElementById("animation-input");
	
	if (show) {
		animationInput.style.display = "flex";
	} else {
		animationInput.style.display = "none";
	}
}

function inputDimensions() {
	const widthInput = document.getElementById("width");
	const heightInput = document.getElementById("height");
	const scaleInput = document.getElementById("scale");
	const width = parseInt(isNaN(widthInput.value) ? 0 : widthInput.value);
	const height = parseInt(isNaN(heightInput.value) ? 0 : heightInput.value);
	const scale = parseFloat(isNaN(scaleInput.value) ? 0 : scaleInput.value);
	
	return [width, height, scale];
}

function inputColors() {
	const relativeColorsInput = document.getElementById("relative-colors");
	const colors = [];
	
	for (let i = 1; i <= 4; i++) {
		const chooser = document.getElementById(`color-${i}`);
		const [r, g, b] = chooser.jscolor.rgb;
		const rgb = (r << 16) | (g << 8) | b;
		
		colors.push(rgb);
	}
	
	return [colors, relativeColorsInput.checked];
}

function inputNoiseType() {
	const distanceSlider = document.getElementById("noise-distance-type");
	const valueSlider = document.getElementById("noise-value-type");
	const distanceType = parseInt(distanceSlider.value);
	const valueType = parseInt(valueSlider.value);
	
	return [distanceType, valueType];
}

function inputAnimation() {
	const animateInput = document.getElementById("animate");
	const rangeInput = document.getElementById("animation-range");
	const stepInput = document.getElementById("animation-step");
	const animate = animateInput.checked;
	const range = parseFloat(rangeInput.value);
	const step = parseFloat(stepInput.value);
	
	return [animate, range, step];
}

function generate(worley) {
	const canvas = document.getElementById("canvas");
	const [width, height, scale] = inputDimensions();
	const [distanceType, valueType] = inputNoiseType();
	const [colors, relativeColors] = inputColors();
	const [animate, range, step] = inputAnimation();
	
	canvas.width = width;
	canvas.height = height;
	
	const g = canvas.getContext("2d");
	const canvasData = g.createImageData(width, height);
	let data;
	
	if (!animate) {
		data = worley.generate_noise_images(width, height, scale, colors, relativeColors, distanceType, valueType, 0.0, 0.0);
	} else {
		data = worley.generate_noise_images(width, height, scale, colors, relativeColors, distanceType, valueType, range, step);
	}
	
	prepareDownload(worley, width, height, data);
	
	requestAnimationFrame(() => display(g, canvasData, data, 0, currentAnimationId));
}

function display(g, canvasData, data, index, animationId) {
	if (!animationsPlaying[animationId]) {
		return;
	}
	
	const imageData = data[index];
	
	requestAnimationFrame(() => {
		for (let i = 0; i < canvasData.data.length && i < imageData.length; i++) {
			canvasData.data[i] = imageData[i];
		}
		
		g.putImageData(canvasData, 0, 0);
	});
	
	if (data.length > 1) {
		index = (index + 1) % data.length;
		
		window.setTimeout(() => requestAnimationFrame(() => display(g, canvasData, data, index, animationId)), 100);
	}
}

function prepareDownload(worley, width, height, data) {
	const downloadButton = document.getElementById("download");
	const newDownloadButton = downloadButton.cloneNode(true);
	
	downloadButton.parentNode.replaceChild(newDownloadButton, downloadButton);
	newDownloadButton.addEventListener("click", () => {
		const gifData = worley.create_gif(width, height, data);
		const dataArray = new Uint8Array(gifData);
		const blob = new Blob([dataArray], {
			type: "image/gif"
		});
		const link = document.createElement("a");
		
		document.body.appendChild(link);
		
		link.style.display = "none";
		link.href = URL.createObjectURL(blob);
		link.download = "noise.gif";
		link.click();
	});
}
